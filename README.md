[![Built with Spacemacs](https://cdn.rawgit.com/syl20bnr/spacemacs/442d025779da2f62fc86c2082703697714db6514/assets/spacemacs-badge.svg)](https://develop.spacemacs.org)

# roman number to integer

Converting string of roman numbers to integers in Common Lisp.

This project is for learning purposes only.

There are some checks in place to avoid errors. However, there are no checks if the roman number is formatted wrong.

Argument has to be a string (case insensitive). Return values are an `integer` or `NIL` if the argument is invalid.

## Usage

The main function is `roman-to-integer`:

```lisp
(roman-to-integer "xxix") ; => 29
(roman-to-integer "xxio") ; => NIL
```

In addition, the macro `char-roman-value` can also be used to get the value of a single roman number character:

```lisp
(char-roman-value #\i) ; => 1
(char-roman-value #\o) ; => NIL
```
## License
This software is licensed under the [GPL3](https://www.gnu.org/licenses/gpl-3.0.html) license.
![GPL](https://www.gnu.org/graphics/gplv3-88x31.png "GPL3")
