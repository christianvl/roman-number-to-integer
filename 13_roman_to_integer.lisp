;;;-----------------------------------------------------------------------------
;;; 13. Roman to integer
;;;-----------------------------------------------------------------------------

;;; I, V, X, L, C, D, M

;;; convert a roman number to integer

;;; III = 3; LVIII = 58; MCMXCIV = 1994

(defpackage :roman-to-integer
  (:nicknames :rmn-int)
  (:use :cl))

(in-package :roman-to-integer)

(defmacro is-char (x)
  "Checks if argument is a character"
  `(equal (type-of ,x) 'standard-char))

(defmacro char-is-roman (x)
  "Checks if character argument is a roman-number character"
  `(not (not (find (char-upcase ,x) '(#\I #\V #\X #\L #\C #\D #\M)))))

(defun valid-roman-chars (roman-numb)
  "Checks if a string contains only roman-number characters"
  (loop for c across roman-numb do
    (if (not (and (is-char c) (char-is-roman c)))
        (return-from valid-roman-chars NIL)))
  (return-from valid-roman-chars T))

(defmacro char-roman-value (x)
  "Returns the integer value of a roman character"
  `(cdr (assoc (char-upcase ,x) (pairlis '(#\I #\V #\X #\L #\C #\D #\M)
                                         '(1 5 10 50 100 500 1000)))))

(defun roman-to-integer (roman-numb)
  "Returns the integer value of a roman number string"
  (when (not (valid-roman-chars roman-numb))
    (return-from roman-to-integer NIL))
  (let ((chars-list (loop for c across roman-numb collect c))
        (chars-length (length roman-numb))
        (summ))
    (dotimes (i chars-length)
      (if (and
           (< i (1- chars-length))
           (< (char-roman-value (nth i chars-list))
              (char-roman-value (nth (1+ i) chars-list))))
          (push (* -1 (char-roman-value (nth i chars-list))) summ)
          (push (char-roman-value (nth i chars-list)) summ)))
    (apply #'+ summ)))
