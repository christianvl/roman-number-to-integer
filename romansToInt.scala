package example

/** Map of roman char keys and int values*/
val romans: Map[Char, Int] = Map(
  'I' -> 1,
  'V' -> 5,
  'X' -> 10,
  'L' -> 50,
  'C' -> 100,
  'D' -> 500,
  'M' -> 1000
)

/**
  * Calculates the value of a roman number formatted in absolute values as a List[Int].
  * This is a helper function to be used in romanStringToInt
  *
  * Convert List[Int] to Array(n, n+1), compares if n < n+1
  * and return -n or n accordingly. Finally, add original list last element
  *
  * @param a List[Int] of roman chars absolute values
  * @return Int value of a list of roman chars absolute values
  */
def calculator(a: List[Int]): Int =
  if a.size == 1 then return a.head // can't compare numbers if there's only one.
  var list: scala.collection.mutable.ListBuffer[Int] = scala.collection.mutable.ListBuffer().empty
  a.sliding(2)
    .foreach(i => {
      list += { if i(0) < i(1) then -i(0) else i(0)  }
    })
  list += a.last
  list.reduce(_ + _)

/**
  * Converts a string of roman chars numbers to an Option[Int] value.
  * Invalid chars in String will cause the function to return None.
  *
  * @param roman: String roman number
  * @return value: Option[Int] for a String with valid roman chars.
  */
def romanStringToInt(roman: String): Option[Int] =
  if !(roman.toUpperCase.forall(romans.contains)) then None
  else
    var n: Int = calculator(roman
      .toUpperCase()
      .map(romans(_))
      .toList)
    Some(n)

@main def convert(): Unit =
  val roman = "XXIX"
  val answer = romanStringToInt(roman).getOrElse(0)
  println(s"$roman = $answer")
